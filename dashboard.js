/* globals Chart:false, feather:false */

(() => {
  'use strict'
  feather.replace({ 'aria-hidden': 'true' })

  // Graphs
  const ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: [
        'Januar',
        'Februar',
        'März',
        'April',
        'Mai',
        'Juni',
        'Juli',
        'August',
        'September',
        'Oktober',
        'November',
        'Dezember'
      ],
      datasets: [
          {
            label: 'erzeugte kWh',
        data: [
          188.6,
          374.1,
          1228.4,
          1484.4,
          1704.5,
          1728.0,
          1538.1,
          1586.0,
          984.4,
          750.2,
          274.7,
          127.1
        ],
        lineTension: 0,
        backgroundColor: '#ee840c',
        borderColor: '#ee840c',
        borderWidth: 2,
      },
        {
          label: 'verbrauchte kWh',
        data: [
    961.4,
    771.2,
    813.8,
    623.4,
    581.0,
    541.0,
    521.2,
    575.8,
    495.9,
    550.7,
    739.0,
    885.3
  ],
      lineTension: 0,
      backgroundColor: '#043264',
      borderColor: '#043264',
      borderWidth: 2,
        }
      ]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: true
      }
    }
  })


// ###########################################################################################
  // Graphs
  const ctx1 = document.getElementById('verbrauch')
  // eslint-disable-next-line no-unused-vars
  const verbrauch = new Chart(ctx1, {
    type: 'bar',
    data: {
      labels: [
        'Januar',
        'Februar',
        'März',
        'April',
        'Mai',
        'Juni',
        'Juli',
        'August',
        'September',
        'Oktober',
        'November',
        'Dezember'
      ],
      datasets: [
        {
          label: 'verbrauchte kWh aus Solarenergie',
          data: [
            118.6,
            187.0,
            469.2,
            366.8,
            386.3,
            374.3,
            351.0,
            348.9,
            262.9,
            258.9,
            156.3,
            98.4
          ],
          lineTension: 0,
          backgroundColor: '#ee840c',
          borderColor: '#ee840c',
          borderWidth: 2,
        },
        {
          label: 'verbrauchte kWh aus dem Netz',
          data: [
            843.0,
            584.0,
            345.0,
            257.0,
            195.0,
            167.0,
            170.0,
            227.0,
            233.0,
            292.0,
            583.0,
            787.0
          ],
          lineTension: 0,
          backgroundColor: '#043264',
          borderColor: '#043264',
          borderWidth: 2,
        }
      ]
    },
    options: {
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: true
      }
    }
  })
})()
