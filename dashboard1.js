/* globals Chart:false, feather:false */

(() => {
    'use strict'

    feather.replace({ 'aria-hidden': 'true' })

    // Graphs
    const ctx2 = document.getElementById('speicher')
    // eslint-disable-next-line no-unused-vars
    const speicher = new Chart(ctx2, {
        type: 'line',
        data: {
            labels: [
                '01.01.',
                '31.01.',
                '27.02.',
                '31.03.',
                '30.04.',
                '31.05.',
                '30.06.',
                '31.07.',
                '31.08.',
                '30.09.',
                '31.10.',
                '30.11.',
                '31.12.'
            ],
            datasets: [{
                label: 'kumulierter Solarenergieüberschuss in kWh',
                data: [
                    0,
                    -773,
                    -1170,
                    -742,
                    119,
                    1243,
                    2430,
                    3447,
                    4457,
                    4946,
                    5145,
                    4681,
                    3939
                ],
                lineTension: 0,
                backgroundColor: 'transparent',
                borderColor: '#007bff',
                borderWidth: 4,
                pointBackgroundColor: '#007bff'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            },
            legend: {
                display: true
            }
        }
    })
})()
